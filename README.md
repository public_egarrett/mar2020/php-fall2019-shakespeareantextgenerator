A PHP application implementing Predis. Written for a Fall 2019 course at Dawson College.
Training classes written by Eira Garrett, data retrieval and front end classes written by Camillia Elachqar.

A basic predictive text generator trained on works by William Shakespeare.